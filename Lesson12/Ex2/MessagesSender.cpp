#include "MessagesSender.h"

void MessagesSender::printMenu()
{
	std::cout << "Choose one of the following options: "<< std::endl;
	std::cout << "1. sign in " << std::endl;
	std::cout << "2. sign out " << std::endl;
	std::cout << "3. connected users " << std::endl;
	std::cout << "4. exit" << std::endl;
}

void MessagesSender::signin()
{
	std::string name = "";
	bool taken = false;

	std::cout << "Enter Username: ";
	std::cin >> name;

	for(std::vector<std::string>::iterator i = this->_users.begin(); i != this->_users.end(); i++)
	{
		if(*i == name)
		{
			std::cout << "name already taken" << std::endl;
			taken = true;
			break;
		}
	}

	if (!taken)
	{
		this->_users.push_back(name);
		std::cout << "Added" << std::endl;
	}
}

void MessagesSender::signout()
{
	std::string name = "";
	bool exsists = false;

	std::cout << "Enter Username: ";
	std::cin >> name;

	for (std::vector<std::string>::iterator i = this->_users.begin(); i != this->_users.end(); i++)
	{
		if (*i == name)
		{
			this->_users.erase(i);
			std::cout << "User removed" << std::endl;

			exsists = true;
			break;
		}
	}

	if (!exsists)
	{
		std::cout << "Username does not exsist" << std::endl;
	}
}

void MessagesSender::connectedUsers()
{
	for (std::vector<std::string>::iterator i = this->_users.begin(); i != this->_users.end(); i++)
	{
		std::cout << *i << " " << std::endl;
	}
}

std::vector<std::string> MessagesSender::getUsers()
{
	return this->_users;
}
