#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <iterator>


class MessagesSender
{
public:
	void printMenu();
	void signin();
	void signout();
	void connectedUsers();

	std::vector<std::string> getUsers();

private:
	std::vector<std::string> _users;
};
