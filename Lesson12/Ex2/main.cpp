#include "MessagesSender.h"
#include <queue>
#include <fstream>
#include <mutex>
#include <thread>

#define DATA_FILE "./data.txt"
#define OUT_FILE "./output.txt"
#define WAIT_TIME 60

std::mutex mute;
bool run = true;

void read60s(std::queue<std::string> &q)
{
	std::ifstream fileR;
	std::ofstream fileW;
	std::string line;

	while (run)
	{
		mute.lock();
		fileR.open(DATA_FILE);
		if (fileR.is_open())
		{
			//filling the Queue with each line
			while (std::getline(fileR, line))
			{
				q.push(line);
			}
			fileR.close();

			//emptying the text file
			fileW.open(DATA_FILE);
			fileW.write("", 0);
			fileW.close();
		}
		else
			std::cout << "Unable to open file";
		mute.unlock();
		std::this_thread::sleep_for(std::chrono::seconds(WAIT_TIME));
	}
}

void spreadMsg(std::queue<std::string> &q, MessagesSender &m)
{
	std::string line = "";
	std::ofstream file;

	while (run)
	{
		mute.lock();
		file.open(OUT_FILE);

		for (int i = 0; i < q.size(); i++)
		{
			for (int j = 0; j < m.getUsers().size(); j++)
			{
				line = q.front();
				line = m.getUsers()[j] + " :" + line + "\n";
				file.write(line.c_str(), line.length());
			}
			line = q.front();
			q.pop();
			q.push(line);
		}

		//closing file
		file.close();
		mute.unlock();
		std::this_thread::sleep_for(std::chrono::seconds(WAIT_TIME)); // to prevent livelock
	}
}

int main()
{
	int choice = 0;
	std::queue<std::string> q;
	MessagesSender m;
	
	std::thread t60s(read60s, ref(q));
	std::thread tSpread(spreadMsg, ref(q), std::ref(m));
	
	t60s.detach();
	tSpread.detach();

	while (run)
	{
		m.printMenu();
		std::cin >> choice;

		switch (choice)
		{
		case 1:
			{
				m.signin();
			}break;
		case 2:
			{
				m.signout();
			}break;
		case 3:
			{
				m.connectedUsers();
			}break;
		case 4:
		{
			run = false;
		}break;
		default:
			{
				std::cout << "Invalid option" << std::endl;
			}break;
		}
	}

	system("pause");
	return 0;
}