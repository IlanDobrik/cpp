#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"
#include "stack.h"
#include "linkedList.h"

#include <iostream>
using namespace std;


void reverse(int* nums, unsigned int size)
{
	int i = 0;
	stack* s = initStack(NULL);
	
	for (i = 0; i < size; i++)
	{
		push(s, nums[i]);
	}

	for( i = 0; i < size; i++)
	{
		nums[i] = pop(s);
	}
}

int* reverse10()
{
	int* nums = new int[10];
	int i = 0;

	for(i = 0; i < 10; i++)
	{
		std::cin >> nums[i];
		getchar();
	}

	return nums;
}