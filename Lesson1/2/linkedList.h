#ifndef LINKEDLIST_H
#define LINKEDLIST_H


typedef struct node
{
	unsigned int value;
	node* next;
} node;

node* initNode(node* head, unsigned int value);
void freeList(node* head);
void printNode(node* head);
int length(node* head);


#endif // LINKEDLIST_H