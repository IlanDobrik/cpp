#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "stack.h"
#include "linkedList.h"


stack* initStack(stack* s)
{
	s = new stack;
	s->head = NULL;

	return s;
}

void push(stack* s, unsigned int element)
{
	node* current = NULL;
	node* newNode = NULL;

	if(s)
	{
		if(s->head)
		{
			current = s->head;
			while(current->next)
			{
				current = current->next;
			}
			newNode = initNode(NULL, element);
			current->next = newNode;
		}
		else
		{
			s->head = initNode(NULL, element);
		}
	}
	else
	{
		printf("Stack is empty\n");
	}
}

int pop(stack* s) // Return -1 if stack is empty
{
	node* current = NULL;
	int result = -1;

	if(s && s->head)
	{
		if (!s->head->next)
		{
			result = s->head->value;
			delete s->head;
			s->head = NULL;
		}
		else
		{
			current = s->head;
			while (current->next->next)
			{
				current = current->next;
			}
			result = current->next->value;
			delete current->next;
			current->next = NULL;
		}
	}
	return result;
}

void cleanStack(stack* s)
{
	if(s && s->head)
	{
		freeList(s->head);
	}
	delete s;
	s = NULL;
}