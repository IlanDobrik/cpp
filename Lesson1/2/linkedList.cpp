#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "linkedList.h"

node* initNode(node* head, unsigned int value)
{
	head = new node;
	head->value = value;
	head->next = NULL;

	return head;
}


void printNode(node* head)
{
	node* current = head;

	if(head)
	{
		while (current)
		{
			printf("%d ", current->value);
			current = current->next;
		}
		printf("\n");
	}
	else
	{
		printf("Node is NULL\n");
	}
}

void freeList(node* head)
{
	node* temp;
	while(head)
	{
		temp = head;
		head = head->next;
		delete temp;
		temp = NULL;
	}
	delete head;
	head = NULL;
}