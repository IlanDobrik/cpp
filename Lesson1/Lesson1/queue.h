#ifndef QUEUE_H
#define QUEUE_H


/* a queue contains positive integer values. */
typedef struct queue
{
	unsigned int* values;
	unsigned int size;
	unsigned int index;
} queue;

void initQueue(queue** q, unsigned int size);
void cleanQueue(queue* q);

void enqueue(queue* q, unsigned int newValue);
int dequeue(queue* q); // return element in top of queue, or -1 if empty

/*
Prints the queue struct.
input: queue
output: none
*/
void printQ(queue* q);

#endif /* QUEUE_H */

/*
init - create an array with requested size AND DONT TOUCH IT EVER AFTER
clean - make everything 0
note: check if input is 0 - isnt natural number
*/