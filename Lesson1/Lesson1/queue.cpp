#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "queue.h"


void cleanQueue(queue* q)
{
	unsigned int i = 0;

	if(q)
	{
		for (i = 0; i < q->size; i++)
		{
			q->values[i] = 0;
		}
		q->index = 0;
	}
	else
	{
		printf("Queue is NULL\n");
	}
}

void initQueue(queue** q, unsigned int size)
{
	unsigned int* arr = NULL;

	arr = (unsigned int*)malloc(sizeof(unsigned int)*size);
	*q = (queue*)malloc(sizeof(queue));

	(*q)->values = arr;
	(*q)->size = size;
	(*q)->index = 0;

	cleanQueue(*q);
}

void enqueue(queue* q, unsigned int newValue)
{
	if (q)
	{
		if (newValue == 0)
		{
			printf("Only natural numbers\n");
		}
		else
		{
			if (q->index == q->size)
			{
				printf("Queue is full\n");
			}
			else
			{
				q->values[q->index] = newValue;
				q->index++;
				printf("Added\n");
			}
		}
	}
	else
	{
		printf("Queue is NULL\n");
	}
}

int dequeue(queue* q)
{
	unsigned int  i = 0, value = 0;

	if(q)
	{
		value = q->values[0];
		if(q->size == q->index)
		{
			q->values[q->index-1] = 0;
		}
		else
		{
			if(q->index != 0)
			{
				for (i = 0; i < q->index; i++)
				{
					q->values[i] = q->values[i + 1];
				}
				q->index--;
			}
			else
			{
				value = -1;
			}
		}
		return value;
	}
	return -1;
}


void printQ(queue* q)
{
	int i = 0;
	for( i =0; i < q->size; i++)
	{
		printf("%d ", q->values[i]);
	}
	printf("\nSize  = %d\nIndex = %d\n", q->size,q->index);
}
