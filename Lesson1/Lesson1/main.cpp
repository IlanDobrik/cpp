#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "queue.h"


#define TRUE 1
#define FLASE 0

/*
.
input:
output: none
*/

int main(void)
{
	queue* q = NULL;
	int  i = 0;
	initQueue(&q, 5);
	printQ(q);
	for ( i = 0; i < 10; i++)
	{
		enqueue(q, 0);
	}
	printQ(q);
	printf("%d\n", dequeue(q));
	printQ(q);
	printf("%d %d\n", q->size, q->index);

	getchar();
	return 0;
}
