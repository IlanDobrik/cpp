#ifndef BSNode_H
#define BSNode_H

#include <string>
#include <iostream>

using namespace std;

template <class T>
class BSNode
{
public:
	BSNode(T data);
	BSNode(const BSNode& other);

	~BSNode();
	
	void insert(T value);
	BSNode& operator=(const BSNode& other);

	bool isLeaf() const;
	T getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;

	bool search(T val) const;

	int getHeight() const;
	int getDepth(const BSNode& root) const;

	void printNodes() const; //for question 1 part C

	//My funcs
	int getCount() const;

private:
	T _data;
	BSNode* _left;
	BSNode* _right;

	int _count; //for question 1 part B
	//int getCurrNodeDistFromInputNode(const BSNode* node) const; //auxiliary function for getDepth 
	//didnt use this^^
};

template <class T>
BSNode<T>::BSNode(T data) //V
{
	this->_data = data;

	this->_left = NULL;
	this->_right = NULL;

	this->_count = 1;
}
template <class T>
BSNode<T>::BSNode(const BSNode& other)
{
	*this = other;
}
template <class T>
void BSNode<T>::insert(T value) //V
{
	if (value != this->_data)
	{
		if (!(value > this->_data) && this->_left != NULL)
			this->_left->insert(value);
		if (!(value < this->_data) && this->_right != NULL)
			this->_right->insert(value);

		if (value < this->_data && this->_left == NULL)
			this->_left = new BSNode(value);
		if (value > this->_data && this->_right == NULL)
			this->_right = new BSNode(value);
	}
	else
		this->_count++;
}

template <class T>
BSNode<T>::~BSNode()
{
	if (this->_left != NULL)
		delete this->_left;
	if (this->_right != NULL)
		delete this->_right;
}
template <class T>
BSNode<T>& BSNode<T>::operator=(const BSNode& other)
{
	this->_data = other.getData();
	this->_count = other.getCount();

	if (other.getLeft() != NULL)
		this->_left = new BSNode(*other.getLeft());
	else
		this->_left = NULL;
	if (other.getRight() != NULL)
		this->_right = new BSNode(*other.getRight());
	else
		this->_right = NULL;

	return *this;
}

template <class T>
bool BSNode<T>::isLeaf() const //V
{
	if (this->_left == NULL && this->_right == NULL)
		return true;
	return false;
}
template <class T>
T BSNode<T>::getData() const //V
{
	return this->_data;
}
template <class T>
int BSNode<T>::getCount() const //V
{
	return _count;
}
template <class T>
BSNode<T>* BSNode<T>::getLeft() const //V
{
	return this->_left;
}
template <class T>
BSNode<T>* BSNode<T>::getRight() const //V
{
	return this->_right;
}

template <class T>
bool BSNode<T>::search(T val) const //V
{
	if (this == NULL)
		return false;
	else
		if (this->_data == val)
			return true;
		else
			if (this->_data < val)
				return this->_right->search(val);
			else
				return this->_left->search(val);
}

template <class T>
int BSNode<T>::getHeight() const //V
{
	if (this->_right == NULL && this->_left == NULL)
		return 0;
	if (this->_right == NULL)
		return this->_left->getHeight() + 1;
	if (this->_left == NULL)
		return this->_right->getHeight() + 1;

	return this->_left->getHeight() + this->_right->getHeight() + 1;
}
template <class T>
int BSNode<T>::getDepth(const BSNode& root) const
{
	if (this == &root)
		return 0;
	if (root.getLeft() == NULL && root.getRight() == NULL)
		return -1;

	if (root.getRight() == NULL)
		return this->getDepth(*root.getLeft()) + 1;
	if (root.getLeft() == NULL)
		return this->getDepth(*root.getRight()) + 1;

	return this->getDepth(*root.getRight()) + this->getDepth(*root.getLeft()) + 1;
}

template <class T>
void BSNode<T>::printNodes() const //for question 1 part C
{
	if (this != NULL)
	{
		this->_left->printNodes();
		std::cout << this->_data << " " << this->_count << endl;
		this->_right->printNodes();
	}
}
#endif
