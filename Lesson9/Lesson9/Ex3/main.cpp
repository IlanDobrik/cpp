#include "BSNode.h"
#include <string>
#include <cstdlib>
#include <time.h>

using namespace std;

// very useful ;)
template <class T>
void printArr(T arr[], int size)
{
	for (int i = 0; i < size; i++)
		cout << arr[i] << " ";
	cout << endl;
}

template <class T>
void loadArr(T arr[], int size, BSNode<T>* bs)
{
	for (int i = 1; i < size; i++)
		bs->insert(arr[i]);
}


int main()
{
	srand(time(NULL));

	//randomizing an int array
	int arrInt[15] = { 0 };
	for(int i =0; i< 15;i++)
	{
		arrInt[i] = (rand() % 20);
	}

	//randomizing a strign array
	char alphanum[] = "abcdefghijklmnopqrstuvwxyz";
	string s = "";
	string arrString[15] = { "" };
	for (int i = 0; i < 15; ++i) 
	{
		s = "";
		for (int j = 0; j < 2; j++)
			s += alphanum[rand() % (sizeof(alphanum) - 1)];
		arrString[i] = s;
	}

	//printing the arrays 
	printArr(arrInt , 15);
	printArr(arrString, 15);

	//creating bst
	BSNode<int>* bsInt = new BSNode<int>(arrInt[0]);
	BSNode<string>* bsString = new BSNode<string>(arrString[0]);

	//filling tree's
	loadArr<int>(arrInt, 15, bsInt);
	loadArr<string>(arrString, 15, bsString);

	//printing trees
	bsInt->printNodes();
	cout << endl;
	bsString->printNodes();


	system("pause");
	return 0;
}

