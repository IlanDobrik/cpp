#include "functions.h"
#include "testClass.h"

int main() {

//double
//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;

//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for ( int i = 0; i < arr_size; i++ ) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;

	//char
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<char>(1.0, 2.5) << std::endl;
	std::cout << compare<char>(4.5, 2.4) << std::endl;
	std::cout << compare<char>(4.4, 4.4) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	char charArr[arr_size] = { 'I', 'l', 'a', 'n','D' };
	bubbleSort<char>(charArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << charArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<char>(charArr, arr_size);
	std::cout << std::endl;

	//testClass
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<testClass>(1.0, 2.5) << std::endl;
	std::cout << compare<testClass>(4.5, 2.4) << std::endl;
	std::cout << compare<testClass>(4.4, 4.4) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	testClass testArr[arr_size] = { 'I', 'l', 'a', 'n','D' };
	bubbleSort<testClass>(testArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << testArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<testClass>(testArr, arr_size);
	std::cout << std::endl;
	system("pause");
	return 1;
}