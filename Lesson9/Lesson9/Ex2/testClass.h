#pragma once
#include <iostream>

class testClass
{
public:
	testClass(int value);
	testClass();
	~testClass();

	int _value;

	void operator=(int value);
	bool operator<(testClass& other);
	bool operator>(testClass& other);
	bool operator==(testClass& other);

	friend std::ostream& operator<<(std::ostream& os, const testClass& other);
};