#pragma once
#include <iostream>

template <class  T>
int compare(T x, T y)
{
	if (x > y)
		return -1;
	if (x < y)
		return 1;
	return 0;
}

template <class  T>
void bubbleSort(T arr[], int n)
{
	T temp = 0;
	for (int i = 0; i < n - 1; i++)
		for (int j = 0; j < n - i - 1; j++)
			if (arr[j] > arr[j + 1])
			{
				temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
				
}

template <class  T>
void printArray(T arr[], int n)
{
	for (int i = 0; i < n; i++)
		std::cout << arr[i] << std::endl;
}
