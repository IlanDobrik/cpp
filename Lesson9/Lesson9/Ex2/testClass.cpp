#include "testClass.h"


testClass::testClass(int value)
{
	this->_value = value;
}
testClass::testClass()
{
	this->_value = 0;
}
testClass::~testClass()
{
}

void testClass::operator=(int value)
{
	this->_value = value;
}

bool testClass::operator<(testClass& other)
{
	return this->_value < other._value;
}
bool testClass::operator>(testClass& other)
{
	return this->_value > other._value;
}

bool testClass::operator==(testClass& other)
{
	return this->_value == other._value;
}

std::ostream& operator<<(std::ostream& os, const testClass& other)
{
	os << other._value;
	return os;
}