#include "BSNode.h"
#include <fstream>
#include <string>
#include <windows.h>
#include "printTreeToFile.h"
using namespace std;


int main()
{
	BSNode* bs = new BSNode("6");
	bs->insert("2");
	bs->insert("8");
	bs->insert("3");
	bs->insert("5");
	bs->insert("9");
	bs->insert("6");

	BSNode* bs2 = new BSNode(*bs);
	bs2->insert("6");
	bs2->printNodes();
	cout << endl;
	bs->printNodes();

	//checking isLeaf
	cout << "Leaf : " << bs->isLeaf() << endl;
	cout << "Leaf : " << bs->getRight()->getRight()->isLeaf() << endl << endl;

	//checking search
	cout << "search: " << bs->search("13") << endl;
	cout << "search: " << bs->search("5") << endl;
	cout << "search: " << bs->search("9") << endl;
	cout << "search: " << bs->search("6") << endl;
	cout << "search: " << bs->search("3") << endl << endl;

	//checking _count
	cout << "count: " << bs->getCount() << endl;
	cout << "count: " << bs->getRight()->getCount() << endl << endl;

	BSNode* s = new BSNode("7");
	//checking getDepth
	cout << "Depth: " << bs->getDepth(*s) << endl;
	cout << "Depth: " << bs->getLeft()->getDepth(*s) << endl;

	cout << "Depth: " << bs->getDepth(*bs) << endl;
	cout << "Depth: " << bs->getLeft()->getDepth(*bs) << endl;
	cout << "Depth: " << bs->getLeft()->getRight()->getDepth(*bs) << endl << endl;


	cout << "Tree height: " << bs->getHeight() << endl;
	cout << "depth of node with 5 depth: " << bs->getLeft()->getRight()->getRight()->getDepth(*bs) << endl;
	cout << "depth of node with 3 depth: " << bs->getLeft()->getRight()->getDepth(*bs) << endl << endl;

	bs->printNodes();

	string textTree = "BSTData.txt";
	printTreeToFile(bs, textTree);

	system("BinaryTree.exe");
	system("pause");
	remove(textTree.c_str());
	delete bs;

	return 0;
}

