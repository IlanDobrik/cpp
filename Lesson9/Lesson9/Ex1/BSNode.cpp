#include "BSNode.h"

BSNode::BSNode(string data) //V
{
	this->_data = data;
	
	this->_left = NULL;
	this->_right = NULL;

	this->_count = 1;
}
BSNode::BSNode(const BSNode& other)
{
	*this = other;
}

BSNode::~BSNode()
{
	if (this->_left != NULL)
		delete this->_left;
	if (this->_right != NULL)
		delete this->_right;
}

void BSNode::insert(string value) //V
{
	if(value != this->_data)
	{
		if (!(value > this->_data) && this->_left != NULL)
			this->_left->insert(value);
		if (!(value < this->_data) && this->_right != NULL)
			this->_right->insert(value);

		if (value < this->_data && this->_left == NULL)
			this->_left = new BSNode(value);
		if (value > this->_data && this->_right == NULL)
			this->_right = new BSNode(value);
	}
	else
		this->_count++;
}
BSNode& BSNode::operator=(const BSNode& other)
{
	this->_data = other.getData();
	this->_count = other.getCount();

	if (other.getLeft() != NULL)
		this->_left = new BSNode(*other.getLeft());
	else
		this->_left = NULL;
	if (other.getRight() != NULL)
		this->_right = new BSNode(*other.getRight());
	else
		this->_right = NULL;

	return *this;
}

bool BSNode::isLeaf() const //V
{
	if (this->_left == NULL && this->_right == NULL)
		return true;
	return false;
}
string BSNode::getData() const //V
{
	return this->_data;
}
int BSNode::getCount() const //V
{
	return _count;
}
BSNode* BSNode::getLeft() const //V
{
	return this->_left;
}
BSNode* BSNode::getRight() const //V
{
	return this->_right;
}

bool BSNode::search(string val) const //V
{
	if (this == NULL)
		return false;
	else
		if (this->_data == val)
			return true;
		else
			if (this->_data < val)
				return this->_right->search(val);
			else
				return this->_left->search(val);
}

int BSNode::getHeight() const //V
{
	if (this->_right == NULL && this->_left == NULL)
		return 0;
	if(this->_right == NULL)
		return this->_left->getHeight() + 1;
	if(this->_left == NULL)
		return this->_right->getHeight() + 1;

	return this->_left->getHeight() + this->_right->getHeight() + 1;
}
int BSNode::getDepth(const BSNode& root) const
{
	if(this == &root)
		return 0;
	if (root.getLeft() == NULL && root.getRight() == NULL)
		return -1;

	if (root.getRight() == NULL)
		return this->getDepth(*root.getLeft()) + 1;
	if (root.getLeft() == NULL)
		return this->getDepth(*root.getRight()) + 1;

	return this->getDepth(*root.getRight()) + this->getDepth(*root.getLeft()) +1;
}

void BSNode::printNodes() const //for question 1 part C
{
	if (this != NULL) 
	{
		this->_left->printNodes();
		std::cout << this->_data << " " << this->_count << endl;
		this->_right->printNodes();
	}
}

int BSNode::getCurrNodeDistFromInputNode(const BSNode* node) const //auxiliary function for getDepth
{
	return -1;
}