#include "Nucleus.h"

// Gene
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

// Gene getters
unsigned int Gene::get_start() const
{
	return this->_start;
}

unsigned int Gene::get_end() const
{
	return this->_end;
}

bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}

// Gene setters
void Gene::set_start(const unsigned int new_start)
{
	this->_start = new_start;
}

void Gene::set_end(const unsigned int new_end)
{
	this->_end = new_end;
}

void Gene::set_on_complementary_dna_strand(const bool new_on_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = new_on_complementary_dna_strand;
}


// Nucleus
void Nucleus::init(const std::string dna_sequence)
{
	unsigned int i = 0;
	for (i = 0; i < dna_sequence.length(); i++)
	{
		if( dna_sequence[i] == 'G')
		{
			this->_complementary_DNA_strand += 'C';
		}
		else if (dna_sequence[i] == 'C')
		{
			this->_complementary_DNA_strand += 'G';
		}
		else if (dna_sequence[i] == 'T')
		{
			this->_complementary_DNA_strand += 'A';
		}
		else if (dna_sequence[i] == 'A')
		{
			this->_complementary_DNA_strand += 'T';
		}
		else
		{
			this->_complementary_DNA_strand = "";
			std::cerr << "dna sequence is not valid" << std::endl;
			_exit(1);
		}
	}
	this->_DNA_strand = dna_sequence;
}

std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	unsigned int i = 0;
	std::string RNA = "";

	if (gene.is_on_complementary_dna_strand())
	{
		RNA = this->_complementary_DNA_strand.substr(gene.get_start(), gene.get_end() - gene.get_start() + 1);
	}
	else
	{
		RNA = this->_DNA_strand.substr(gene.get_start(), gene.get_end() - gene.get_start() + 1);
	}
	
	for (i = 0; i < RNA.length(); i++) 
	{
		if(RNA[i] == 'T')
		{
			RNA[i] = 'U';
		}

	}
	return RNA;
}

std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string reversed_DNA = this->_DNA_strand;
	std::reverse(reversed_DNA.begin(), reversed_DNA.end());
	return reversed_DNA;
}

unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	int res = 0, j = 0, i = 0;

	for (i = 0; i <= this->_DNA_strand.length() - codon.length(); i++)
	{
		for (j = 0; j < codon.length(); j++)
		{
			if (this->_DNA_strand[i + j] != codon[j])
			{
				break;
			}
		}
			
		if (j == codon.length())
		{
			res++;
			j = 0;
		}
	}

	return res;
}
