#include "Cell.h"


void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_nucleus.init(dna_sequence);
	this->_mitochondrion.init();
	this->_glocus_receptor_gene = glucose_receptor_gene;
	this->_atp_units = 0;
}

bool Cell::get_ATP()
{
	std::string transcript = "";
	Protein* protein;

	transcript = this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene);
	protein = this->_ribosome.create_protein(transcript);
	if(protein)
	{
		this->_mitochondrion.insert_glucose_receptor(*protein);
		this->_mitochondrion.set_glucose(50);
		if(this->_mitochondrion.produceATP(0))
		{
			this->_atp_units = 100;
		}
		else
		{
			return false;
		}
	}
	else
	{
		std::cerr << "dna sequence is not valid" << std::endl;
		_exit(1);
	}
}
