#include "Mitochondrion.h"

void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}

void Mitochondrion::insert_glucose_receptor(const Protein & protein)
{
	AminoAcidNode* first = protein.get_first();

	if(first && first->get_data() == AminoAcid::ALANINE)
	{
		first = first->get_next();
		if (first && first->get_data() == AminoAcid::LEUCINE)
		{
			first = first->get_next();
			if (first && first->get_data() == AminoAcid::GLYCINE)
			{
				first = first->get_next();
				if (first && first->get_data() == AminoAcid::HISTIDINE)
				{
					first = first->get_next();
					if (first && first->get_data() == AminoAcid::LEUCINE)
					{
						first = first->get_next();
						if (first && first->get_data() == AminoAcid::PHENYLALANINE)
						{
							first = first->get_next();
							if (first->get_data() == AminoAcid::AMINO_CHAIN_END)
							{
								this->_has_glocuse_receptor = true;
							}
						}
					}
				}
			}
		}
	}
}

void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}

bool Mitochondrion::produceATP(const int glocuse_unit) const
{
	return this->_has_glocuse_receptor && this->_glocuse_level >= 50;
}