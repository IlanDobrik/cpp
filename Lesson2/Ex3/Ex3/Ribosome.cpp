#include "Ribosome.h"

Protein* Ribosome::create_protein(std::string &RNA_transcript) const
{
	std::string coding = "";
	Protein* protein = new Protein;
	(*protein).init();
	
	while (RNA_transcript.length() >= 3)
	{
		coding = RNA_transcript.substr(0, 3);
		RNA_transcript = RNA_transcript.substr(3, RNA_transcript.length());

		if (get_amino_acid(coding) == AminoAcid::UNKNOWN)
		{
			(*protein).clear();
			return NULL;
		}
		else
		{
			(*protein).add(get_amino_acid(coding));
		}
	}

	return protein;
}
