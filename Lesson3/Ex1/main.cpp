#include "Vector.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main()
{
	Vector v(5);
	
	for(int i =0;i<5; i++)
	{
		v.push_back(i);
	}

	Vector v2 = v;
	v2.pop_back();
	v.printVector();
	v2.printVector();


	system("pause");
	return 0;
}