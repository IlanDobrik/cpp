#include "Vector.h"

//A
Vector::Vector(int n)
{
	int* arr = nullptr;
	// checking if n is valid
	if(n < 2)
	{
		n = 2;
	}
	// filling in values
	this->_capacity = n;
	this->_resizeFactor = n;
	this->_size = 0;
	arr = new int[n];

	this->_elements = arr;
}
Vector::~Vector()
{
	delete[] this->_elements;
}
int Vector::size() const //return size of vector
{
	return this->_size;
}
int Vector::capacity() const //return capacity of vector
{
	return this->_capacity;
}
int Vector::resizeFactor() const //return vector's resizeFactor
{
	return this->_resizeFactor;
}
bool Vector::empty() const //returns true if size = 0
{
	return this->_size == 0;
}

//B
//Modifiers
void Vector::push_back(const int& val)  //adds element at the end of the vector
{
	//checking if theres a need to add more capacity
	if(this->_size >= this->_capacity)
	{
		int* newArr = new int[this->_capacity + this->_resizeFactor];
		this->_capacity += this->_resizeFactor;
		
		// copying values to new arr
		for (int i = 0; i < this->_size; i++)
		{
			newArr[i] = this->_elements[i];
		}
		//freeing unused memory
		delete[] this->_elements;

		this->_elements = newArr;
	}
	// adding the new value
	this->_elements[this->_size] = val;
	this->_size++;
}
int Vector::pop_back()  //removes and returns the last element of the vector
{
	// checking if vector is empty or not
	if (!this->_size)
	{
		std::cout << "error: pop from empty vector" << std::endl;
		return -9999;
	}
	return this->_elements[this->_size--];
}
void Vector::reserve(int n)  //change the capacity
{
	int* newArr = nullptr;
	int newCap = 0;
	// checking if n is valid
	if(this->_capacity < n)
	{
		// creatign the new array
		newCap = this->_capacity;
		while(newCap < n)
		{
			newCap += this->_resizeFactor;
		}
		newArr = new int[newCap];
		this->_capacity = newCap;

		// cpoying values
		for(int i = 0; i < this->_capacity; i++)
		{
			newArr[i] = this->_elements[i];
		}
		// freeing memory
		delete[] this->_elements;

		this->_elements = newArr;
	}
	else
	{
		std::cout << "Could not reserve more space. Given paramater is smaller than Vector's capacity" << std::endl;
	}
}
void Vector::resize(int n)  //change _size to n, unless n is greater than the vector's capacity
{
	// checking if theres a need to add more capacity to the vector
	if(this->_capacity < n)
	{
		this->reserve(n);
	}
	this->_size = n;
}
void Vector::assign(int val)  //assigns val to all elemnts
{
	for(int i = 0; i < this->_size; i++)
	{
		this->_elements[i] = val;
	}
}
void Vector::resize(int n, const int& val)  //same as above, if new elements added their value is val
{
	int oldSize = this->_size;
	this->resize(n);
	// cpoying values to new space created
	for(int i = oldSize; i < n; i++)
	{
		this->_elements[i] = val;
	}
}

//C
//The big three (d'tor is above)
Vector::Vector(const Vector& other)
{
	// copying values
	this->_capacity = other.capacity();
	this->_size = other.size();
	this->_resizeFactor = other.resizeFactor();

	// setting up new array for deep copy
	int* newArr = new int[this->_capacity];
	for(int i = 0; i < this->_capacity; i ++)
	{
		newArr[i] = other._elements[i];
	}
	this->_elements = newArr;
}
Vector& Vector::operator=(const Vector& other)
{
	Vector v(other);
	return v;
}

//D
//Element Access
int& Vector::operator[](int n) const  //n'th element
{
	// checking if n is valid
	if (n > this->_size - 1 || n < 0)
	{
		std::cout << "Invalid position" << std::endl;
		return this->_elements[0];
	}
	return this->_elements[n];
}

// Additional
void Vector::printVector()
{
	for (int i = 0; i < this->_size; i++)
	{
		std::cout << this->_elements[i] << " ";
	}
	std::cout << std::endl;
}
