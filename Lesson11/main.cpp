#include "threads.h"

int main()
{
	call_I_Love_Threads();
	
	vector<int> primes1;
	getPrimes(58, 100, primes1);
	printVector(primes1);
	primes1 = callGetPrimes(93, 289);
	
	vector<int> primes2 = callGetPrimes(0, 1000);
	vector<int> primes3 = callGetPrimes(0, 100000);
	vector<int> primes4 = callGetPrimes(0, 1000000);

	callWritePrimesMultipleThreads(0, 1000, "primes2.txt", 2);
	callWritePrimesMultipleThreads(0, 100000, "primes3.txt", 2);
	callWritePrimesMultipleThreads(0, 1000000, "primes4.txt", 2);


	system("pause");
	return 0;
}