#include "threads.h"

void I_Love_Threads()
{
	cout << "I Love Threads" << endl;
}

void call_I_Love_Threads()
{
	std::thread t(I_Love_Threads);
	t.join();
}

void getPrimes(int begin, int end, vector<int>& primes)
{
	int j = 0;
	bool isPrime = true;

	// prime numbers only start past 2
	if (begin < 2)
		begin = 2;

	for(int i = begin; i <= end; i++)
	{
		j = 2;
		isPrime = true;

		// going over the deviders of the number.
		// if the number has a devider, i block it from entering the vector and break the while to conserve in time
		while (j <= sqrt(i))
		{
			if (i % j == 0)
			{
				isPrime = false;
				break;
			}
			j++;
		}

		if (isPrime)
			primes.push_back(i);
	}
}


void printVector(vector<int> primes)
{
	for (vector<int>::iterator i = primes.begin(); i != primes.end(); i++)
	{
		cout << *i << endl;
	}
}

vector<int> callGetPrimes(int begin, int end)
{
	vector<int> v;
	std::thread t(getPrimes, begin, end, ref(v));
	std::clock_t start;

	start = std::clock();
	t.join();
	cout << "Duration: " << std::clock() - start << endl;

	return v;
}

void writePrimesToFile(int begin, int end, ofstream& file)
{
	vector<int> v = callGetPrimes(begin, end);
	string data = "";

	for (int i = 0; i < v.size(); i++)
	{
		data += to_string(v[i]) + "\n";
		// Hey ben. so in this part of the debugging i came to a very stage penomenon
		// as u can see, running the program like it is now, will write to the file just fine.
		// but if u were to chnage in this function the "\n" to " "(space)
		//and look again at the file, u will see what i struggled with.
		// primes2.txt look like is should
		// though 3 and 4.....seem...... a bit off.
		// i would like it if u explained why that happens.
	}

	file.write(data.c_str(), data.length());
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	std::clock_t start;
	ofstream file;
	int sectionEnd = 0;
	
	file.open(filePath);
	start = std::clock();
	
	for (int i = begin; i < end; i += end/N)
	{
		//checking if the end of the section doesnt continue past the end given. if it does, set it back to the end 
		sectionEnd = i + end / N;
		if (sectionEnd > end)
			sectionEnd = end;

		//opening thread
		std::thread t(writePrimesToFile, i, sectionEnd, ref(file));
		t.join();	
	}

	cout << "Duration: " << std::clock() - start << endl;
	file.close();
}
