#include "Circle.h"

void Circle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLUE[] = { 0, 0, 255 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLUE, 100.0f).display(disp);
}

void Circle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLACK, 100.0f).display(disp); // was missing for somereason?
}
Circle::Circle(const Point& center, double radius, const string& type, const string& name) :Shape(name, type)
{
	this->_center += center;
	this->_radius = radius;
}
// getters
const Point& Circle::getCenter() const
{
	return this->_center;
}
double Circle::getRadius() const
{
	return this->_radius;
}
double Circle::getArea() const // pi*r^2
{
	// formula
	return PI * pow(this->_radius, 2);
}
double Circle::getPerimeter() const//2*pi*r
{
	// formula
	return 2 * PI*this->_radius;
}
void Circle::move(const Point& other)
{
	// using point's add func to change circle's posiion
	this->_center += other;
}
