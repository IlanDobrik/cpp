#include "Point.h"

//defult constructor
Point::Point()
{
	this->_x = 0;
	this->_y = 0;
}

Point::Point(double x, double y)
{
	this->_x = x;
	this->_y = y;
}
Point::Point(const Point& other)
{
	*this = other;
}
Point::~Point()
{
}

Point Point::operator+(const Point& other) const
{
	// creating new point of the sum of the previous and given point
	double newX = this->_x + other.getX(), newY = this->_y + other.getY();
	Point p(newX, newY);
	return p;
}
Point& Point::operator+=(const Point& other)
{
	// using operator+ to shorten this func
	*this = *this + other;
	return *this;
}
double Point::getX() const
{
	return this->_x;
}
double Point::getY() const
{
	return this->_y;
}
void Point::setX(const int x)
{
	this->_x = x;
}
void Point::setY(const int y)
{
	this->_y = y;
}
double Point::distance(const Point& other) const
{
	// pitagoras formula
	return sqrt(pow(this->_x - other.getX(), 2) + pow(this->_y - other.getY(), 2));
}
