#include "Triangle.h"



void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}
Triangle::Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name):Polygon(name, type)
{
	//storing all the points
	this->_points.push_back(a);
	this->_points.push_back(b);
	this->_points.push_back(c);
}
double Triangle::getArea() const
{
	//formula for triangle's area
	return 0.5*(this->_points[0].getX()*(this->_points[1].getY()- this->_points[2].getY()) + this->_points[1].getX()*(this->_points[2].getY() - this->_points[0].getY()) + this->_points[2].getX()*(this->_points[0].getY() - this->_points[1].getY()));
}
double Triangle::getPerimeter() const
{
	//adding up the sides
	return this->_points[0].distance(this->_points[1]) + this->_points[1].distance(this->_points[2]) + this->_points[0].distance(this->_points[2]);
}
void Triangle::move(const Point& other)
{
	//getting a point and by a loop adding it to each point
	for(int i =0; i < 3;i++)
		this->_points[i] += other;
}
