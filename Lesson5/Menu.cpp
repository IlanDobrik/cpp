#include "Menu.h"

// some enum's for the menu's
enum SHAPES {
	CIRCLE, ARROW, TRIANGLE, RECTANGLE
};
enum OPTIONS {
	ADD, MODIFY, DELETE_ALL
};
enum MODIFY_OPTIONS {
	MOVE, DETAIL, REMOVE
};
// constructor
Menu::Menu() 
{
	_board  = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
}
// destructor
Menu::~Menu()
{
	_disp->close();
	delete _board;
	delete _disp;
}

// the main program AKA the whole project
void Menu::menu()
{
	int choice = 0;

	while (choice != 3)
	{
		// input choice
		choice = 0;
		cout << "Enter 0 to add a new shape." << endl << "Enter 1 to modify or get information from a current shape." << endl << "Enter 2 to delete all of the shapes." << endl << "Enter 3 to exit." << endl;
		cin >> choice;

		// choosing what to do with the choice given
		switch (choice)
		{
		case ADD:
		{
			addShape(addShapeMenu());
		}break;
		case MODIFY:
		{
			modifyShape();
		}break;
		case DELETE_ALL:
		{
			// clearing the board and deleting the vector's elements
			cleanBoard();
			this->_shapes.clear();
		}break;
		}
		// updating the board
		drawBoard();
	}
}
int Menu::addShapeMenu()
{
	// getting choice
	int choice = -1;
	cout << "Enter 0 to add a circle." << endl << "Enter 1 to add an arrow." << endl << "Enter 2 to add a triangle." << endl << "Enter 3 to add a rectangle." << endl;
	cin >> choice;
	return choice;
}

void Menu::addShape(const int choice)
{
	// choosing what shape to create
	switch (choice)
	{
	case CIRCLE:
	{
		this->_shapes.push_back(addCircle());
	}break;
	case ARROW:
	{
		this->_shapes.push_back(addArrow());
	}break;
	case TRIANGLE:
	{
		// if shape has invalid arguments dont add to the vector
		Triangle* t = addTriangle();
		if (t->getType() != "Invalid")
			this->_shapes.push_back(t);
		else
			delete t;
	}break;
	case RECTANGLE:
	{
		myShapes::Rectangle* r = addRectangle();
		if (r->getType() != "Invalid")
			this->_shapes.push_back(r);
		else
			delete r;
	}break;
	default:
	{
		break;
	}
	}
}
void Menu::modifyShape()
{
	// if there are no shapes, dont allow to continue and return back to the main menu 
	if (this->_shapes.size() == 0)
		cout << "No shapes" << endl;
	else
	{
		int index = -1, choice = -1;
		// printing the options and in return getting the index of the shape in the vector
		for (int i = 0; i < this->_shapes.size(); i++)
			cout << "Enter " << i << " for " << this->_shapes[i]->getName() << "(" << this->_shapes[i]->getType() << ")" << endl;
		cin >> index;
		// printing and getting what to do with the shape
		cout << "Enter 0 to move the shape" << endl << "Enter 1 to get its details." << endl << "Enter 2 to remove the shape." << endl;
		cin >> choice;

		// sending the shape to the correct func
		switch (choice)
		{
		case MOVE:
		{
			double x = 0, y = 0;
			cout << "Please enter the X moving scale:" << endl;
			cin >> x;

			cout << "Please enter the Y moving scale:" << endl;
			cin >> y;
			
			cleanBoard();
			this->_shapes[index]->move(Point(x, y));
		}break;
		case DETAIL:
		{
			this->_shapes[index]->printDetails();
		}break;
		case REMOVE:
		{
			cleanBoard();
			delete this->_shapes[index];
			this->_shapes.erase(this->_shapes.begin() + index);
		}break;
		}
	}
}

// creating and returning circle
Circle* Menu::addCircle()
{
	string name = "";
	double x = 0, y = 0, radius = 0;

	cout << "Please enter X: " << endl;
	cin >> x;

	cout << "Please enter Y: " << endl;
	cin >> y;
	Point center(x, y);

	cout << "Please enter radius: " << endl;
	cin >> radius;

	cout << "Please enter the name of the shape: " << endl;
	cin >> name;

	Circle* c = new Circle(center, radius, "Circle", name);
	return c;
}
// creatign and returning an arrow
Arrow* Menu::addArrow()
{
	double x = 0, y = 0;
	string name = "";

	cout << "Enter the X of point number: 1" << endl;
	cin >> x;
	cout << "Enter the Y of point number: 1" << endl;
	cin >> y;
	Point p1(x, y);

	cout << "Enter the X of point number: 2" << endl;
	cin >> x;
	cout << "Enter the Y of point number: 2" << endl;
	cin >> y;
	Point p2(x, y);

	cout << "Please enter the name of the shape: " << endl;
	cin >> name;

	Arrow * a = new Arrow(p1, p2, "Arrow", name);
	return a;
}
// creating and returning a triangle
Triangle* Menu::addTriangle()
{
	double x = 0, y = 0;
	string name = "";

	cout << "Enter the X of point number: 1" << endl;
	cin >> x;
	cout << "Enter the Y of point number: 1" << endl;
	cin >> y;
	Point p1(x, y);

	cout << "Enter the X of point number: 2" << endl;
	cin >> x;
	cout << "Enter the Y of point number: 2" << endl;
	cin >> y;
	Point p2(x, y);

	cout << "Enter the X of point number: 3" << endl;
	cin >> x;
	cout << "Enter the Y of point number: 3" << endl;
	cin >> y;
	Point p3(x, y);

	cout << "Please enter the name of the shape: ";
	cin >> name;

	string type = "Triangle";
	// if points goven make a line. change type to invlaid and delete it later on
	if ((p1.getX() == p2.getX() && p1.getX() == p3.getX()) || (p1.getY() == p2.getY() && p1.getY() == p3.getY()))
	{
		cout << "The points entered create a line." << endl;
		type = "Invalid";
	}
	Triangle* t = new Triangle(p1, p2, p3, name, type);
	return t;
}
// creating and returning a rectangle
myShapes::Rectangle* Menu::addRectangle()
{
	string name = "";
	double x = 0, y = 0, length = 0, width = 0;

	cout << "Enter the X of the to left corner: " << endl;
	cin >> x;

	cout << "Enter the Y of the to left corner:" << endl;
	cin >> y;
	Point topLeft(x, y);

	cout << "Please enter the length of the shape:" << endl;
	cin >> length;

	cout << "Please enter the width of the shape:" << endl;
	cin >> width;

	cout << "Please enter the name of the shape: ";
	cin >> name;

	string type = "Rectangle";
	// checking if the inputed values are posiable. if not change type to invalid and delete later on
	if(width == 0 || length == 0)
	{
		cout << "Length or Width can't be 0." << endl;
		type = "Invlaid";
	}
	myShapes::Rectangle* r = new myShapes::Rectangle(topLeft, length, width, name, type);
	return r;
}

//cleaning the paint board from all shapes
void Menu::cleanBoard()
{
	for (int i = 0; i < this->_shapes.size(); i++)
		this->_shapes[i]->clearDraw(*this->_disp, *this->_board);
}

//drawing all shapes on the board
void Menu::drawBoard()
{
	for (int i = 0; i < this->_shapes.size(); i++)
		this->_shapes[i]->draw(*this->_disp, *this->_board);
}