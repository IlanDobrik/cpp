#include "Shape.h"
#include <string>

// do i need to comment this file??
Shape::Shape(const string& name, const string& type) 
{
	this->_name = name;
	this->_type = type;
}
void Shape::printDetails() const
{
	cout << "Name: " << this->_name << "\nType: " << this->_type << "\nArea: " <<this->getArea() << "\nPerimeter: " << this->getPerimeter() << endl;
}
string Shape::getType() const
{
	return this->_type;
}
string Shape::getName() const
{
	return this->_name;
}
