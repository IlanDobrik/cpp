#include "Rectangle.h"

void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char WHITE[] = { 255, 255, 255 };
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), WHITE, 100.0f).display(disp);
}

void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0};
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);
}
myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const string& type, const string& name):Polygon(name, type)
{
	// pushing the top left, and bottom right corner of the rectangle so the print function will work properly
	Point p0(a.getX(), a.getY());
	this->_points.push_back(p0);//point 0

	Point p3(a.getX() + width, a.getY()-length);
	this->_points.push_back(p3);//point 3
	/*
	Points order
	0---------------1
	|				|
	|				|
	|				|
	2---------------3
	*/
}
double myShapes::Rectangle::getArea() const
{
	// formula to calculate rectangle's area
	return this->_points[0].distance(this->_points[1])*this->_points[0].distance(this->_points[2]);
}
double myShapes::Rectangle::getPerimeter() const
{
	// formula to calculate rectangle's perimeter
	return 2*this->_points[0].distance(this->_points[1]) + 2*this->_points[0].distance(this->_points[2]);
}
void myShapes::Rectangle::move(const Point& other)
{
	// adding the point to all points of the rectangle
	for (int i = 0; i < 2; i++)
		this->_points[i] += other;
}

