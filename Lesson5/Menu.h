#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>


class Menu
{
public:

	Menu();
	~Menu();

	// more functions..
	void menu();

private: 
	int addShapeMenu();
	void addShape(const int choice);

	Circle* addCircle();
	Arrow* addArrow();
	Triangle* addTriangle();
	myShapes::Rectangle* addRectangle();

	void modifyShape();

	void cleanBoard();
	void drawBoard();

	vector<Shape*> _shapes;
	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
};

