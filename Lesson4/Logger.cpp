#include "Logger.h"

unsigned int Logger::_counter = 0;

void Logger::setStartLine()
{
	_counter++;
	this->_startLine = true;
}
Logger::Logger()
{
	OutStream a;
	this->os = a;
	this->_startLine = true;
}
Logger::~Logger()
{
}

Logger& operator<<(Logger& l, const char *msg)
{
	//cheking if im at a new line
	if (l._startLine)
	{
		l.os << "LOG" << l._counter;
	}
	// printing msg
	l.os << msg;
	l._startLine = false; // not on new line anymore 
	return l;
}
Logger& operator<<(Logger& l, int num)
{
	//cheking if im at a new line
	if(l._startLine)
	{
		l.os << "LOG" << l._counter;
	}
	//printing num
	l.os << num;
	l._startLine = false;// not on new line anymore 
	return l;
}
Logger& operator<<(Logger& l, void(*pf)(FILE* file))
{
	//cheking if im at a new line
	if (l._startLine)
	{
		l.os << "LOG" << l._counter;
	}
	// starting new line
	l.os << endline;
	l.setStartLine();

	return l;
}
