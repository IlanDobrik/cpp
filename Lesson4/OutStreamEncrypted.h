#pragma once
#include "OutStream.h"
#include <string>

class OutStreamEncrypted : public OutStream
{
private:
	int _key;
public:
	OutStreamEncrypted(int key);
	~OutStreamEncrypted();

	OutStreamEncrypted& operator<<(const char *str);
	OutStreamEncrypted& operator<<(int num);
	OutStreamEncrypted& operator<<(void(*pf)(FILE* file));
};
