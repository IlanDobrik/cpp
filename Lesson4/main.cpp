#include "OutStream.h"
#include "FileStream.h"
#include "OutStreamEncrypted.h"
#include "Logger.h"
#include <iostream>


int main(int argc, char **argv)
{
	//Question A Part B
	OutStream a;
	a << "I am the Doctor and I'm " << 1500 << " years old" << endline;
	//Question A part D
	FILE* file = fopen("Test.txt", "w");
	FileStream b(file);
	b << "I am the Doctor and I'm " << 1500 << " years old" << endline;

	OutStreamEncrypted c(11);
	c << "I am the Doctor and I'm " << 1500 << " years old" << endline;
	//log
	Logger d;
	d << "I an the Doctor and I'm ";
	d << 1500;
	d << "years old";
	d << endline;
	d << "test"<<endline;
	d << "13"<<endline;



	system("pause");
	return 0;
}
