#include "OutStreamEncrypted.h"
#include <string>
#include <iostream>

OutStreamEncrypted::OutStreamEncrypted(int key)
{
	this->_key = key;
	this->_file = stdout;
}
OutStreamEncrypted::~OutStreamEncrypted()
{
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(const char *str)
{
	int temp = 0;
	char* encrypted = new char[strlen(str)]; // str is const so i need new string

	// going letter by letter and encrypting them
	for (int i = 0; i < strlen(str); i++)
	{
		temp = (int)str[i] + this->_key;
		// if the letter is above the ascii limit given, i reset it to "loop" back
		if (temp > 126)
		{
			temp = temp - 126 + 32;
		}
		// if the letter is below the ascii limit, i reset it to "loop" back to the end
		else if (temp < 32)
		{
			temp = temp - 32 + 126;
		}
		encrypted[i] = (char)temp;
	}
	// making it a string insted of char array
	encrypted[strlen(str)] = 0;

	// printing
	OutStream a;
	a << encrypted;
	//delete[] encrypted 
	return *this;
}
OutStreamEncrypted& OutStreamEncrypted::operator<<(int num)
{
	int i = 0;
	std::string encrypted = std::to_string(num);
	char* temp = new char[encrypted.length()];

	// going letter by letter and encrypting them
	for (i = 0; i < encrypted.length(); i++)
	{
		encrypted[i] += this->_key;
		// if the letter is above the ascii limit given, i reset it to "loop" back
		if (encrypted[i] > 126)
		{
			encrypted[i] = encrypted[i] - 126 + 32;
		}
		// if the letter is below the ascii limit, i reset it to "loop" back to the end
		else if (encrypted[i] < 32)
		{
			encrypted[i] = encrypted[i] - 32 + 126;
		}
		temp[i] = encrypted[i];
	}
	temp[i] = 0;

	//printing
	OutStream a;
	a << temp;
	return *this;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(void(*pf)(FILE* file))
{
	OutStream a;
	a << endline;
	return *this;
}
