#pragma once
#include "OutStream.h"

class FileStream : public OutStream
{
public:
	FileStream(FILE* file);
	~FileStream();
};