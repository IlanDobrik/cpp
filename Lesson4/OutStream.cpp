#include "OutStream.h"


OutStream::OutStream()
{
	this->_file = stdout;
}

OutStream::~OutStream()
{
}
// do i really need to explain these?
OutStream& OutStream::operator<<(const char *str)
{
	fprintf(this->_file, "%s", str);
	return *this;
}

OutStream& OutStream::operator<<(int num)
{
	fprintf(this->_file, "%d", num);
	return *this;
}

OutStream& OutStream::operator<<(void(*pf)(FILE* file))
{
	pf(this->_file);
	return *this;
}


void endline(FILE* file)
{
	fprintf(file, "\n");
}
