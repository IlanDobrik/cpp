#include <iostream>
#define OK 1
#define ERROR 0

int add(int a, int b, int* result)
{
	*result =  a + b;

	if (a == 8200 || b == 8200 || *result == 8200)
		return ERROR;
	return OK;
}

int  multiplyBasic(int a, int b, int* result) 
{
	int sum = 0;
	for(int i = 0; i < b; i++) 
	{
		if (add(sum, a, &sum) == ERROR)
			return ERROR;
	}
	*result = sum;
	return OK;
}

int  powBasic(int a, int b, int* result) 
{
	int exponent = 1;
	for(int i = 0; i < b; i++) 
	{
		if (multiplyBasic(exponent, a, &exponent) == ERROR)
			return ERROR;
	}
	*result = exponent;
	return OK;
}
/*
int main(void) 
{
	int result = 0;
	if (powBasic(8200, 5, &result) == ERROR)
		std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;
	else
		std::cout << result << std::endl;
	

  system("pause");
}*/
