#include <iostream>
using namespace std;

#define PI 3.14


int main()
{

	unsigned int size = 0, i = size;
	
	int t1 = 0, t2 = 1, nextTerm;

	cout << "what is the size of the series? ";
	cin >> size;

	i = size;
	while (i >= 1)
	{
		nextTerm = t1 + t2;
		printf("%d, ", t1);
		t1 = t2;
		t2 = nextTerm;

		i--;
	}

	system("Pause");
	return 0;
}
