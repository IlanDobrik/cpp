#include <iostream>
#pragma pack(1)
using namespace std;


typedef struct  STUDENT
{
	char class_type;
	short math_grade;
	short english_grade;
	short bible_grade;
	int age;

} STUDENT;

int main () {

	STUDENT* student = new(STUDENT);
	student->class_type = 'A';
	student->age = 3;
	student->math_grade = 100;
	student->english_grade = 100;
	student->bible_grade = 100;


	printf("age is: %d\n", student->age);

	char* age = (char*)student + sizeof(char) + sizeof(short) + sizeof(short) + sizeof(short);
	printf("age is: %d\n", *(int*)age);

	system("Pause");
	return 0;
}
