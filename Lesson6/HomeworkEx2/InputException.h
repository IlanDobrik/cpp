#pragma once
#include <exception>

class InputException : public std::exception
{
public:
	virtual const char* what() const;
};

const char* InputException::what() const
{
	return "Input Exception!\n";
}