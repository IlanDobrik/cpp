#include "Hexagon.h"

void Hexagon::draw()
{
	std::cout << "Name is " << getName() << std::endl << "Color is" << getColor() << std::endl << "Side length is " << getSide() << std::endl << "Area is " << CalArea() << std::endl;
}
double Hexagon::CalArea() 
{
	return MathUtils::CalHexagonArea(this->length);
}
Hexagon::Hexagon(std::string name, std::string color, double length): Shape(name, color)
{
	setSide(length);
}
void Hexagon::setSide(double length) 
{
	this->length = length;
}
double Hexagon::getSide()
{
	return this->length;
}