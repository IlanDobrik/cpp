#pragma once
#include "MathUtils.h"
#include "Shape.h"

class Hexagon : public Shape
{
public:
	void draw();
	double CalArea();
	Hexagon(std::string, std::string, double);
	void setSide(double length);
	double getSide();

private:
	double length;
};