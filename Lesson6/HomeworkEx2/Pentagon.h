#pragma once
#include "MathUtils.h"
#include "Shape.h"

class Pentagon : public Shape
{
public:
	void draw();
	double CalArea();
	Pentagon(std::string, std::string, double);
	void setSide(double length);
	double getSide();

private:
	double length;
};