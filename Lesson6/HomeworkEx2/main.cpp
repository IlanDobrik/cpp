#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"
#include "InputException.h"
#include "Hexagon.h"
#include "Pentagon.h"

int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180, length = 0; int height = 0, width = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);

	Hexagon hex(nam, col, length);
	Pentagon pen(nam, col, length);

	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	
	Shape* ptrhex = &hex;
	Shape* ptrpen = &pen;


	
	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p'; std::string shapetype;
	char x = 'y';
	while (x != 'x') 
	{
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, h = Hexagon, a = Pentagon" << std::endl;
		std::cin >> shapetype;
		if (shapetype.length() > 1)
			std::cout << "Warning - Don't try to build more than one shape at once" << std::endl;
		try
		{
			switch (shapetype[0]) 
			{
			case 'h':
				std::cout << "enter color, name,  side length" << std::endl;
				std::cin >> col >> nam >> length;
				if (std::cin.fail())
					throw InputException();
				if (length < 0)
					throw shapeException();
				hex.setColor(col);
				hex.setName(nam);
				hex.setSide(length);
				ptrhex->draw();
				break;
			case 'a':
				std::cout << "enter color, name,  side length" << std::endl;
				std::cin >> col >> nam >> length;
				if (std::cin.fail())
					throw InputException();
				if (length < 0)
					throw shapeException();
				pen.setColor(col);
				pen.setName(nam);
				pen.setSide(length);
				ptrpen->draw();
				break;
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam >> rad;
				if (std::cin.fail())
					throw InputException();
				if (rad < 0)
					throw shapeException();
				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (std::cin.fail())
					throw InputException();
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (std::cin.fail())
					throw InputException();
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				if (std::cin.fail())
					throw InputException();
				if (ang + ang2 != 180)
					throw shapeException();
				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();

			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin >> x;
		}
		catch(InputException &e)
		{
			std::cin.clear();
			getchar();
			std::cout << e.what();
		}
		catch (std::exception &e)
		{			
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}



		system("pause");
		return 0;
	
}