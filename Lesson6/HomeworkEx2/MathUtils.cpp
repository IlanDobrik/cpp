#include "MathUtils.h"


double MathUtils::CalHexagonArea(double length)
{
	return ((3 * sqrt(3))/2)*pow(length, 2);
}

double MathUtils::CalPentagonArea(double length) 
{
	return 0.25*sqrt(5*(5 + 2*sqrt(5)))*pow(length, 2);
}
