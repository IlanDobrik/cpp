#pragma once
#include <math.h>

class MathUtils
{
public:
	static double CalPentagonArea(double length);
	static double CalHexagonArea(double length);
};
