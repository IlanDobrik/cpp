#include "Pentagon.h"

void Pentagon::draw()
{
	std::cout << "Name is " << getName() << std::endl << "Color is" << getColor() << std::endl << "Side length is " << getSide() << std::endl << "Area is " << CalArea() << std::endl;
}
double Pentagon::CalArea()
{
	return MathUtils::CalPentagonArea(this->length);
}
Pentagon::Pentagon(std::string name, std::string color, double length):Shape(name, color)
{
	setSide(length);
}
void Pentagon::setSide(double length)
{
	this->length = length;
}
double Pentagon::getSide()
{
	return this->length;
}