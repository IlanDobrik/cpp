#include "Shape.h"
#include "Triangle.h"
#include <iostream>

using namespace std;


int main() {

	Shape * triangle = new Triangle(2, 10);

	cout << "The area of the triangle is " << triangle->get_area() << '\n';


	Triangle triangleObject(1, 10);

	cout << "The area of the triangle is " << triangleObject.get_area() << '\n';

	system("pause");
	return 0;
}