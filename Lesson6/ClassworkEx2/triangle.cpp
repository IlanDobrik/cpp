#include "Shape.h"
#include "Triangle.h"
#include <iostream>

using namespace std;


Triangle::Triangle(float base, float height) 
{
	this->base = base;
	this->height = height;
}

float Triangle::get_area(bool has_depth) {
	return (0.5*base*height);
}