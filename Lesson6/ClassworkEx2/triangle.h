#ifndef TRIANGLE_H
#define TRIANGLE_H
#include "shape.h"

class Triangle : public Shape {

public:
	Triangle(float base, float height);
	virtual float get_area(bool has_depth = false);

private:
	float base, height;

};


#endif 