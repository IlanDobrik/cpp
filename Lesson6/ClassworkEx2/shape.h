#ifndef SHAPE_H
#define SHAPE_H

class Shape {	

public:
	Shape();
	virtual float get_area(bool has_depth = false);

private:
	float area, perimeter;

};

#endif 