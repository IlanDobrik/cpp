#include "Customer.h"

Customer::Customer(string name)
{
	this->_name = name;
}

Customer::Customer()
{
	this->_name = "Bob";
}

Customer::~Customer()
{
}

double Customer::totalSum() const
{
	double sum = 0;
	for (set<Item>::iterator curr = this->_items.begin(); curr != this->_items.end(); curr++)
	{
		sum += curr->getCount() * curr->getUnitPrice();
	}
	return sum;
}

void Customer::addItem(Item item)
{
	int temp = 0;
	for (set<Item>::iterator i = this->_items.begin(); i != this->_items.end(); i++)
	{
		if(i->getSerialNum() == item.getSerialNum())
		{
			temp = item.getCount();
			item.setCount(temp + i->getCount());

			this->_items.erase(i);
			this->_items.insert(item);
			return;
		}
	}
	this->_items.insert(item);
}

void Customer::removeItem(Item item)
{
	int temp = 0;
	for (set<Item>::iterator curr = this->_items.begin(); curr != this->_items.end(); curr++)
	{
		if (curr->getSerialNum() == item.getSerialNum())
		{
			temp = curr->getCount();
			this->_items.erase(curr);
			if (--temp > 0)
			{
				item.setCount(temp);
				this->_items.insert(item);
			}
			return;
		}
	}
}

string Customer::getName() const
{
	return this->_name;
}

set<Item> Customer::getItems() const
{
	return this->_items;
}

void Customer::setName(const string name)
{
	this->_name = name;
}

void Customer::setItems(const set<Item> s)
{
	this->_items = s;
}
