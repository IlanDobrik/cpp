#include"Customer.h"
#include<map>

map<string, Customer> abcCustomers;
Item itemList[10] = {
	Item("Milk","00001",5.3),
	Item("Cookies","00002",12.6),
	Item("bread","00003",8.9),
	Item("chocolate","00004",7.0),
	Item("cheese","00005",15.3),
	Item("rice","00006",6.2),
	Item("fish", "00008", 31.65),
	Item("chicken","00007",25.99),
	Item("cucumber","00009",1.21),
	Item("tomato","00010",2.32) };

int welcomeScreen()
{
	int choice = 0;
	cout << "Welcome to MagshiMart!" << endl;
	cout << "1.      to sign as customer and buy items" << endl;
	cout << "2.      to uptade existing customer's items" << endl;
	cout << "3.      to print the customer who pays the most" << endl;
	cout << "4.      to exit" << endl;

	cin >> choice;

	return choice;
}

void optionOne()
{
	string name = "";
	int size = abcCustomers.size(), choice = 0;
	Customer cust("bob");

	cout << "Enter name: ";
	cin >> name;

	abcCustomers[name];
	if (abcCustomers.size() - size == 0) // if the key was added, the size will change.
	{
		cout << "Name already used" << endl;
	}
	else
	{
		abcCustomers.erase(name); // earasing the input i entered
		cust.setName(name);

		for (int i = 0; i < 10; i++)
			cout << i+1 << ". " << itemList[i].getName() << " " << itemList[i].getSerialNum() << " " << itemList[i].getUnitPrice() << endl;


		cin >> choice;
		while(choice != 0)
		{
			cust.addItem(itemList[choice - 1]);
			cin >> choice;
		}
		

		abcCustomers[name] = cust;
	}
}

void optionTwo()
{
	string name = "";
	int choice = 0;
	Customer cust;
	set<Item> s;

	cout << "Enter name: ";
	cin >> name;

	cust = abcCustomers[name];
	if (cust.getName() == "Bob")
		cout << "Name not found" << endl;
	else
	{
		s = cust.getItems();
		for (set<Item>::iterator i = s.begin(); i != s.end(); i++)
			cout << i->getName() << " " << i->getCount() << endl;
		cout << endl;

		while(choice != 3)
		{
			//menu <here>
			cout << "1.      Add items " << endl;
			cout << "2.      Remove items" << endl;
			cout << "3.      Back to menu" << endl;
			cin >> choice;

			switch(choice)
			{
			case 1:
				// printing items to buy
				for (int i = 0; i < 10; i++)
					cout << i + 1 << ". " << itemList[i].getName() << " " << itemList[i].getSerialNum() << " " << itemList[i].getUnitPrice() << endl;
				cout << "Press 0 to exit" << endl;

				//getting items to add 
				cin >> choice;
				while (choice != 0)
				{
					cust.addItem(itemList[choice - 1]);
					cin >> choice;
				}
				//adding
				abcCustomers[name] = cust;
				break;
			case 2:
				// printing items to buy
				for (int i = 0; i < 10; i++)
					cout << i + 1 << ". " << itemList[i].getName() << " " << itemList[i].getSerialNum() << " " << itemList[i].getUnitPrice() << endl;
				cout << "Press 0 to exit" << endl;

				//getting items to add 
				cin >> choice;
				while (choice != 0)
				{
					cust.removeItem(itemList[choice - 1]);
					cin >> choice;
				}
				abcCustomers[name] = cust;
				break;
			case 3:
				break;
			default:
				cout << "Invalid option" << endl;
			}
		}
	}
}

void optionThree()
{
	double sum = 0, max = 0;
	string name = "No registered customers";
	set<Item> s;

	for (map<string, Customer>::iterator customers = abcCustomers.begin(); customers != abcCustomers.end(); customers++)
	{
		s = customers->second.getItems();
		sum = 0;
		for (set<Item>::iterator items = s.begin(); items != s.end(); items++)
		{
			sum += items->getUnitPrice() * items->getCount();
		}
		if (sum > max)
		{
			name = customers->second.getName();
			max = sum;
		}
	}

	cout << name << " " << max << endl;
}

int main()
{
	int choice = 0;
	

	while (choice != 4)
	{
		choice = welcomeScreen();
		switch (choice)
		{
		case 1:
			optionOne(); break;
		case 2:
			optionTwo(); break;
		case 3:
			optionThree(); break;
		case 4:
			break;
		default:
			cout << "Invalid Choice" << endl; break;
		}
	}
	cout << "Bye bye" << endl;

	system("pause");
	return 0;
}
