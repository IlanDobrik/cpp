#include "Item.h"

Item::Item(string name, string sNum, double price)
{
	if (sNum.length() == 5)
		this->_serialNumber = sNum;
	else
		throw "serial number consists of 5 numbers";
	if (price > 0)
		this->_unitPrice = price;
	else
		throw "price must always be bigger than 0!";

	this->_name = name;
	this->_count = 1;
}

Item::~Item()
{
}

double Item::totalPrice() const
{
	return this->_unitPrice * this->_count;
}

bool Item::operator<(const Item & other) const
{
	return this->_serialNumber < other._serialNumber;
}

bool Item::operator>(const Item & other) const
{
	return this->_serialNumber > other._serialNumber;
}

bool Item::operator==(const Item & other) const
{
	return this->_serialNumber == other._serialNumber;
}

string Item::getName() const
{
	return this->_name;
}

string Item::getSerialNum() const
{
	return this->_serialNumber;
}

int Item::getCount() const
{
	return this->_count;
}

double Item::getUnitPrice() const
{
	return this->_unitPrice;
}

void Item::setName(const string name)
{
	this->_name = name;
}

void Item::setSerialNum(const string sNum)
{
	this->_serialNumber = sNum;
}

void Item::setCount(const int count)
{
	this->_count = count;
}

void Item::setUnitPrice(const double price)
{
	this->_unitPrice = price;
}

