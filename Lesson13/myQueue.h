#pragma once
#include <iostream>
#include <queue>

class myQueue
{
public:
	myQueue();
	myQueue(int t);
	~myQueue();

	std::string front();
	std::string pop();
	void push(std::string s);
	void addThread();
	void removeThread();
	int size();

	int getCount();
	int getThreads();

private:
	std::queue<std::string> _q;
	int _threads;
	int _count;
};