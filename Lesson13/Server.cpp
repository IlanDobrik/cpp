#include "Server.h"
#define FILE_PATH "./doc.txt"

//locks
std::mutex mute;
std::mutex userL;
std::mutex fileL;

Server::Server()
{
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET,  SOCK_STREAM,  IPPROTO_TCP); 

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{
	
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	
	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}


void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	// the function that handle the conversation with the client
	std::thread t([this, client_socket] {this->clientHandler(client_socket); });
	t.detach();
}

std::string Server::fitToLength(int num, int length)
{
	// uses the input number and length to create a fetting string
	// for exeample:
	//num = 15, length = 5
	//will return 00015 (length 5, value 15)
	std::string s = std::to_string(num);
	for (int i = 0; i < length - (int)std::to_string(num).length(); i++)
		s = "0" + s;
	return s;
}

std::string Server::msg101(std::string name)
{
	fileL.lock();//file being used
	std::ifstream file(FILE_PATH);
	std::string msg((std::istreambuf_iterator<char>(file)),
		(std::istreambuf_iterator<char>()));
	int position = 0;

	//setting up the messege
	msg = "101" + fitToLength(msg.size(), 5) + msg; 
	msg += fitToLength(name.length(), 2) + name;
	
	//finding the next user and the user's position
	userL.lock(); // _users is being used here
	for(int position = 0; position < this->_users.size(); position++)
	{
		if (this->_users[position] == name)
		{
			if(++position < this->_users.size())
			{
				msg += fitToLength(this->_users[position].length(), 2) + this->_users[position] + std::to_string(position);
			}
			else
			{
				msg += "00" + std::to_string(position);
			}
			break;
		}
	}
	userL.unlock();//_user is not being used any more
	file.close();
	fileL.unlock();//file not being used
	return msg;
}

void Server::resposeHandler(std::string response, std::string name, SOCKET cs, int lineNum)
{
	//getting client's messege type
	int type = std::stoi(response.substr(0, 3));
	std::string msg = "";
	std::ofstream file;

	mute.unlock(); //once a thread got here, meaning he got his uniqe number,he allowes other threads to get their own number

	switch (type)
	{
	case 207:
		{
			userL.lock();// _user being used
			if (lineNum == 0) // to make sure only one thread pushed the 1st user to the back
			{
				msg = this->_users[0];
				this->_users.erase(this->_users.begin());
				this->_users.push_back(msg);
			}
			userL.unlock();//_user no used any more
			//not breaking here because type 204 is continuetion of 207
		}
	case 204:
		{
		//writing to file
		fileL.lock();

		file.open(FILE_PATH);
		file.write(response.substr(8, 8 + atoi(response.substr(3, 5).c_str())).c_str(), atoi(response.substr(3, 5).c_str()));
		file.close();

		fileL.unlock();
		}break;
	case 208:
		break;//is done right aftergetting the response

	default:
		return;
		break;
	}
	msg = msg101(name);
	send(cs, msg.c_str(), msg.size(), 0);
}

void Server::clientHandler(SOCKET clientSocket)
{
	char m[100008]; // maximum posiable size client can send me + 1 (to make it a string)
	std::string response = "", msg = "", name = "";
	int count = 0, lineNum = 0;
	bool run = true;

	try
	{
		this->_messeges.addThread();
		
		count = recv(clientSocket, m, 100007, 0);
		//turning the in comping resopse to string(easier to work with)
		m[count] = 0;
		response = m;
		//pushing the username to a vector
		name = response.substr(5, count);
		userL.lock();
		this->_users.push_back(name);
		userL.unlock();

		msg = msg101(name);
		send(clientSocket, msg.c_str(), msg.size(), 0);  // last parameter: flag. for us will be 0.
		
		do
		{
			response = "133";

			if(name == this->_users[0])
			{
				count = recv(clientSocket, m, 100007, 0);
				m[count] = 0;
				response = m;
				if (response == "208")
					run = false;
				this->_messeges.push(response);
				this->_messeges.pop(); 
			}
			else
			{
				if (this->_messeges.size() > 0)
					response = this->_messeges.pop();
			}

			mute.lock(); // to make sure each thread gets its own number
			lineNum = this->_messeges.getCount();
			resposeHandler(response, name, clientSocket, lineNum);
		} while (run);

		// Closing the socket (in the level of the TCP protocol)
		closesocket(clientSocket);
		
		// _users is being used 
		userL.lock();
		for (std::vector<std::string>::iterator it = this->_users.begin(); it != this->_users.end(); it++)
			if (*it == name)
			{
				this->_users.erase(it);
				break;
			}
		this->_messeges.removeThread();
		userL.unlock();
	}
	catch (const std::exception& e)
	{
		closesocket(clientSocket);
		// _users is being used 
		userL.lock();
		for (std::vector<std::string>::iterator it = this->_users.begin(); it != this->_users.end(); it++)
			if (*it == name)
			{
				this->_users.erase(it);
				break;
			}
		this->_messeges.removeThread();
		userL.unlock();
	}
}

