#include "myQueue.h"

myQueue::myQueue()
{
	this->_threads = 0;
	this->_count = 0;
	this->_q = std::queue<std::string>();
}

myQueue::myQueue(int t)
{
	this->_threads = t;
	this->_count = 0;
	this->_q = std::queue<std::string>();
}

myQueue::~myQueue()
{
}

std::string myQueue::front()
{
	return this->_q.front();
}

std::string myQueue::pop()
{
	std::string s = this->_q.front();
	this->_count++;
	if (this->_count >= this->_threads)
	{
		this->_q.pop();
		this->_count = 0;
	}
	return s;
}

void myQueue::push(std::string s)
{
	this->_q.push(s);
}

void myQueue::addThread()
{
	this->_threads++;
}

void myQueue::removeThread()
{
	this->_threads--;
}
 
int myQueue::size()
{
	return this->_q.size();
}

int myQueue::getCount()
{
	return this->_count;
}

int myQueue::getThreads()
{
	return this->_threads;
}
