#pragma once

#include "myQueue.h"
#include <WinSock2.h>
#include <Windows.h>

#include <iostream>
#include <exception>
#include <string>
#include <fstream>

#include <thread>
#include <mutex>

class Server
{
public:
	Server();
	~Server();
	void serve(int port);
	void clientHandler(SOCKET clientSocket);

private:
	void accept();
	

	std::string fitToLength(int num, int length);
	std::string msg101(std::string name);
	void resposeHandler(std::string response, std::string name, SOCKET cs, int lineNum);

	std::vector<std::string> _users;
	myQueue _messeges;
	SOCKET _serverSocket;
};

